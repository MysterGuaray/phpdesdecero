<!DOCTYPE html>
<html>
<head>
	<title>POO Avanzado</title>
</head>
<body>

	<?php

		define('INC', '/includes/');
		require_once __DIR__ . INC .'Curso.inc.php';

		$curso1 = new Curso('POO en PHP', 'Yesi days', '3 sesiones', 10, true );
		$curso2 = new Curso('Javascript desde cero', 'Alvaro Felipe', '3 sesiones', 0, true );

		echo $curso1->obtenerTitulo();
		echo $curso2->obtenerTitulo();
	?>

</body>
</html>